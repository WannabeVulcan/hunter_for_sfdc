({
    sendtohunter : function(component){
        var action = component.get("c.hunterKnownEmailAddresses");
        action.setCallback(this, function(response){
            console.log(response);
            var state = response.getState();
            if (component.isValid() && state==="SUCCESS"){
                console.log("message sent off to Apex");
                var respo = response.getReturnValue();
                // respo = JSON.stringify(respo, undefined, 2);
                // alert(respo);
                // showAndPopulateModal();
                var newHTML = "<p>Hunter knows of the following email addresses: </p>";
                newHTML += "<p>" + respo + "</p>";
                document.getElementById("display_message").innerHTML = newHTML;
                document.getElementById("hunter_info").style.display = "block";
                

            } else if (state ==="ERROR"){
                var errors = response.getError();
                console.log("ERROR MESSAGE: " + errors.message);
            } else {
                console.log("Action state returned was: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})
