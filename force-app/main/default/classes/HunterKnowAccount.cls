public with sharing class HunterKnowAccount {
    @AuraEnabled
    public static String hunterKnownEmailAddresses() {
        // Send an HTTP request.

        Http h = new Http();

         // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();


        // dev vulcan is the linode, the IP address is the Azure server.
        req.setEndpoint('https://api.hunter.io/v2/email-count?domain=exosite.com');
        req.setMethod('GET');

        // req.setEndpoint('http://dev.wannabevulcan.us/run/default/query');
        // req.setEndpoint('http://52.173.78.41:5002/calculus/query');
        // req.setEndpoint('http://www.yahoo.com');
        // req.setMethod('POST');
        // req.setHeader('x-lc-key', '09D58729-7C7C-4BF2-801B-52B601328D4C' );
        // req.setHeader('content-type', 'application/json' );
        // req.setBody('{"Name": "STRING","Search ID": "STRING","Keyword Set": "STRING","City": "STRING","Density": "STRING"}');

        // Send the request, and return a response
        HttpResponse res = h.send(req);

        System.debug(res.getBody());
        return res.getBody();
    }
}